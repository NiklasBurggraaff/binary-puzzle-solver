//
//  Game.swift
//  Binary Puzzle Solver
//
//  Created by Niklas Burggraaff on 09/12/2019.
//  Copyright © 2019 Niklas Burggraaff. All rights reserved.
//

func columns(_ game: [[Int]]) -> [[Int]] {
    var cols: [[Int]] = []
    for j in Array(0..<game.count) {
        cols.append([])
        for row in game {
            cols[j].append(row[j])
        }
    }
    return cols
}


func finishedRow(_ row: [Int]) -> Bool {
    var prev = [-1, -1]
    for x in row {
        if x != -1 && prev == [x,x] {
            return false
        }
        prev = [prev[1], x]
    }
    let counts = Dictionary(row.map {($0, 1)}, uniquingKeysWith: +)
    return row.allSatisfy({$0 != -1}) && counts[0] == row.count/2
}

func finishedGame(_ game: [[Int]]) -> Bool {
    return game.allSatisfy({finishedRow($0)}) && columns(game).allSatisfy({finishedRow($0)})
}


func checkRow(_ row: [Int]) -> Bool {
    var prev = [-1, -1]
    for x in row {
        if x != -1 && prev == [x,x] {
            return false
        }
        prev = [prev[1], x]
    }
    let counts = Dictionary(row.map {($0, 1)}, uniquingKeysWith: +)
    return counts[0] ?? 0 <= row.count/2 && counts[1] ?? 0 <= row.count/2
}

func checkGame(_ game: [[Int]]) -> Bool {
    return game.allSatisfy({checkRow($0)}) && columns(game).allSatisfy({checkRow($0)})
}


func solveGame(_ game: [[Int]]) -> [[Int]] {
    var games = [game]
    while !finishedGame(games[0]) && !games.isEmpty {
        var g1 = games[0]
        var g2 = games[0]
        outer: for (i, row) in games[0].enumerated() {
            for (j, x) in row.enumerated() {
                if x == -1 {
                    g1[i][j] = 0
                    g2[i][j] = 1
                    break outer
                }
            }
        }
        games.removeFirst()
        if checkGame(g2) {
            games.insert(g2, at: 0)
        }
        if checkGame(g1) {
            games.insert(g1, at: 0)
        }
    }
    if games.count > 0 {
        return games[0]
    } else {
        return []
    }
}

func solveAllGames(_ game: [[Int]]) -> [[[Int]]] {
    var games = [game]
    var finishedGames: [[[Int]]] = []
    while !games.isEmpty {
        var g1 = games[0]
        var g2 = games[0]
        outer: for (i, row) in games[0].enumerated() {
            for (j, x) in row.enumerated() {
                if x == -1 {
                    g1[i][j] = 0
                    g2[i][j] = 1
                    break outer
                }
            }
        }
        
        games.removeFirst()
        
        if finishedGame(g2) {
            finishedGames.append(g2)
        } else if checkGame(g2) {
            games.insert(g2, at: 0)
        }
        
        if finishedGame(g1) {
            finishedGames.append(g1)
        } else if checkGame(g1) {
            games.insert(g1, at: 0)
        }
    }
    
    return finishedGames
}
