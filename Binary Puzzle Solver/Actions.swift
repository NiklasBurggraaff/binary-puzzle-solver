//
//  Actions.swift
//  Binary Puzzle Solver
//
//  Created by Niklas Burggraaff on 09/12/2019.
//  Copyright © 2019 Niklas Burggraaff. All rights reserved.
//

func buttonPressed(_ game: [[Int]], _ i: Int, _ j: Int) -> [[Int]] {
    var returnGame = game
    if returnGame[i][j] == -1 {
        returnGame[i][j] = 0
    } else if returnGame[i][j] == 0 {
        returnGame[i][j] = 1
    } else {
        returnGame[i][j] = -1
    }
    return returnGame
}

func showValue(_ x: Int) -> String {
    if x == -1 {
        return ""
    } else {
        return String(x)
    }
}

func solveGamePressed(_ game: [[Int]]) -> [[Int]] {
    let solvedGame = solveGame(game)
    if solvedGame == [] {
        return game
    } else {
        return solvedGame
    }
}
