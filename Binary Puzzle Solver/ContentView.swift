//
//  ContentView.swift
//  Binary Puzzle Solver
//
//  Created by Niklas Burggraaff on 08/12/2019.
//  Copyright © 2019 Niklas Burggraaff. All rights reserved.
//

import SwiftUI
import Combine

var nVar = 10

var xsMax = Array(repeating: Array(repeating: -1, count: nVar), count: nVar)

var xsCleared = Array(repeating: Array(repeating: -1, count: nVar), count: nVar)
var xsClearedMax = Array(repeating: Array(repeating: -1, count: nVar), count: nVar)

struct ContentView: View {
    @State var n = nVar
    @State var xs: [[Int]] = Array(repeating: Array(repeating: -1, count: nVar), count: nVar)
    @State var finished = false
    @State var check = true
    @State var cleared = false


    var body: some View {
        VStack {
            ForEach(0 ..< self.n, id: \.self) { i in
                HStack {
                    ForEach(0 ..< self.n, id: \.self) { j in
                        Button(action: {
                            self.xs = buttonPressed(self.xs, i, j)
                            self.finished = finishedGame(self.xs)
                            self.check = checkGame(self.xs)
                            self.cleared = false
                        },label: {
                            Text(showValue(self.xs[i][j]))
                        })
                        .frame(width: 30, height: 30)
                        .foregroundColor(!self.check ? Color.red : self.finished ? Color.green : Color.black)
                    }
                }
            }
            HStack {
                Stepper(onIncrement: {
                    self.n += 2
                    var xsNew: [[Int]] = Array(repeating: Array(repeating: -1, count: self.n), count: self.n)
                    if xsMax.count >= self.n {
                        for (i, row) in xsNew.enumerated() {
                            if i >= self.xs.count {
                                xsNew[i] = xsMax[i]
                            } else {
                                for (j, _) in row.enumerated() {
                                    if j < self.xs.count {
                                        xsNew[i][j] = self.xs[i][j]
                                        xsMax[i][j] = self.xs[i][j]
                                    } else {
                                        xsNew[i][j] = xsMax[i][j]
                                    }
                                }
                            }
                        }
                    } else {
                        for (i, row) in self.xs.enumerated() {
                            for (j, x) in row.enumerated() {
                                xsNew[i][j] = x
                            }
                        }
                        xsMax = xsNew
                    }
                    self.xs = xsNew
                    self.finished = finishedGame(self.xs)
                    self.check = checkGame(self.xs)
                    self.cleared = false
                }, onDecrement: {
                    if self.n > 2 {
                        self.n -= 2
                        var xsNew: [[Int]] = Array(repeating: Array(repeating: -1, count: self.n), count: self.n)
                        for (i, row) in xsNew.enumerated() {
                            for (j, _) in row.enumerated() {
                                xsNew[i][j] = self.xs[i][j]
                            }
                        }
                        for (i, row) in self.xs.enumerated() {
                            for (j, x) in row.enumerated() {
                                xsMax[i][j] = x
                            }
                        }
                        self.xs = xsNew
                        self.finished = finishedGame(self.xs)
                        self.check = checkGame(self.xs)
                        self.cleared = false
                    }
                }, label: {
                    Text("Game Size: \(self.n)")
                }).padding(4)
                Button(action:{
                    if self.check {
                        let solution = solveGamePressed(self.xs)
                        self.xs = solution
                        self.finished = finishedGame(self.xs)
                        self.check = checkGame(self.xs)
                        self.cleared = false
                    }
                }, label: {
                    Text("Solve Game")
                }).padding(4)
                Button(action:{
                    if !self.cleared {
                        xsCleared = self.xs
                        xsClearedMax = xsMax
                        self.xs = Array(repeating: Array(repeating: -1, count: self.n), count: self.n)
                        xsMax = Array(repeating: Array(repeating: -1, count: self.n), count: self.n)
                    } else {
                        self.xs = xsCleared
                        xsMax = xsClearedMax
                    }
                    self.cleared = !self.cleared
                }, label: {
                    Text("\(!self.cleared ? "Clear Game" : "Undo Clear")")
                }).padding(4)
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

