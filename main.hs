import Data.List


type Row = [Int]
type Game = [Row]

game :: Game
game =
  [[-1,0,-1,-1,-1,-1,1,-1],
   [-1,0,-1,-1,-1,0,-1,-1],
   [-1,-1,-1,-1,-1,-1,1,0],
   [-1,-1,-1,-1,-1,-1,-1,-1],
   [1,-1,-1,-1,1,-1,-1,1],
   [-1,-1,-1,-1,1,-1,-1,-1],
   [-1,0,-1,-1,-1,0,-1,0],
   [-1,-1,-1,-1,-1,-1,-1,0]]

game1 :: Game
game1 =
  [[-1,-1,-1,-1,-1,-1,-1, 1,-1,-1],
   [-1, 0, 0,-1,-1, 0,-1,-1, 1,-1],
   [-1, 0,-1,-1, 1,-1,-1, 0,-1, 0],
   [-1,-1, 1,-1,-1,-1, 1,-1,-1,-1],
   [ 1,-1, 1,-1,-1,-1,-1,-1,-1, 1],
   [-1,-1,-1,-1,-1,-1,-1, 1,-1,-1],
   [-1, 0,-1,-1, 1,-1,-1,-1, 0,-1],
   [-1,-1,-1,-1, 1, 1,-1,-1,-1, 0],
   [-1, 0,-1, 0,-1,-1, 1,-1,-1, 0],
   [ 0,-1,-1,-1, 0,-1,-1,-1, 1,-1]]

game2 :: Game
game2 =
  [[ 1,-1,-1, 0,-1,-1],
   [-1,-1, 0, 0,-1, 1],
   [-1, 0, 0,-1,-1, 1],
   [-1,-1,-1,-1,-1,-1],
   [ 0, 0,-1, 1,-1,-1],
   [-1, 1,-1,-1, 0, 0]]


columns :: Game -> Game
columns rows = [[row!!i | row <- rows] | i <- [0..length rows - 1]]

finishedRow :: Row -> Bool
finishedRow xs =
  and [x /= (-1) | x <- xs] &&
  and [a /= b || b /= c | (a,b,c) <- zip3 xs (tail xs) (drop 2 xs)] &&
  length [x | x <- xs, x == 1] == (length xs `div` 2)

finishedGame :: Game -> Bool
finishedGame rows = and [finishedRow row | row <- rows] && and [finishedRow col | col <- columns rows]

checkRow :: Row -> Bool
checkRow xs =
  and [a /= b || b /= c | (a,b,c) <- zip3 xs (tail xs) (drop 2 xs), b /= (-1)] &&
  length [x | x <- xs, x == 1] <= (length xs `div` 2) &&
  length [x | x <- xs, x == 0] <= (length xs `div` 2)

checkGame :: Game -> Bool
checkGame rows = and [checkRow row | row <- rows] && and [checkRow col | col <- columns rows]

posGames :: Game -> [Game]
posGames game | and (map (and . map (/=(-1))) game) = [game]
              | otherwise                         =
  [[[if r == i && c == j then 0 else x | (x, c) <- zip row [0..]] | (row, r) <- zip game [0..]],
   [[if r == i && c == j then 1 else x | (x, c) <- zip row [0..]] | (row, r) <- zip game [0..]]]
    where
      (i,j) = firstEmpty game

firstEmpty :: Game -> (Int, Int)
firstEmpty game = first [[(i,j) | (x, j) <- zip row [0..], x == -1] | (row, i) <- zip game [0..], (-1) `elem` row]
  where
    first ((x:xs):xss) = x

solveGame :: Game -> [Game]
solveGame game | finishedGame game = [game]
               | otherwise     = concat $ map solveGame $ filter checkGame $ posGames game

showGame :: [Game] -> IO ()
showGame = putStrLn . unlines . map (\g -> (unlines $ map (unwords . map show) g))
